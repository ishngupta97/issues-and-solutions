## Walkthrough of creating an encironment with custom lalsuite

*January 30, 2025*

#### Create environment

1. conda create -n SMA_NRSur python=3.12
2. conda activate SMA_NRSur
3. cd $CONDA_PREFIX

#### Clone and build custom lalsuite

4. mkdir src opt
5. cd src
6. git lfs install
7. git clone git@git.ligo.org:samson.leong/lalsuite-nr-sur.git
This is the custom lalsuite.
8. cd lalsuite-nr-sur
9. Create a new branch- git checkout SMA_NRSur
10. Now, I want to download the requisite packages.
```
Change env name in common/conda/environment.yml to SMA_NRSur
conda env update -n SMA_NRSur --file common/conda/environment.yml
```
11. Build LALSuite
```
export IDIR=/ligo/software/ligo.org/ish.gupta/conda-envs/SMA_NRSur/opt/lalsuite-nr-sur
rm ${IDIR} -rf && ./00boot && ./configure --prefix=${IDIR} --enable-swig-python --enable-mpi --enable-openmp && make -j8 && make -j8 install
```

#### Install lalsuite-extra

12. Clone `lalsuite-extra` in src and get the `NRSur7dq4` file.
```
git lfs install --skip-smudge
In src, git clone git@git.ligo.org:lscsoft/lalsuite-extra.git
git lfs pull -I data/lalsimulation/NRSur7dq4.h5
```


#### Make sure that the environment variables are set accordingly.

13. When in the main environment directory (where src and opt are present), execute 

```
vim etc/conda/activate.d/env_vars.sh 
```

and edit (for ICS)

```
#!/bin/sh

source /ligo/software/ligo.org/ish.gupta/conda-envs/SMA_NRSur/opt/lalsuite-nr-sur/etc/lalsuite-user-env.sh
LAL_DATA_PATH=/ligo/software/ligo.org/ish.gupta/conda-envs/SMA_NRSur/src/lalsuite-extra/data/lalsimulation/

```

And then make sure the variables are unset when you deactivate
```
vim etc/conda/deactivate.d/env_vars.sh
```
and edit (for ICS)
```
#!/bin/sh

unset LAL_PREFIX
unset PYTHONPATH
unset LAL_DATA_PATH
```

#### Install bilby_tgr and bilby_pipe

14. Now, install `bilby_tgr` in src- `git clone git@git.ligo.org:lscsoft/bilby_tgr.git`
15. cd bilby_tgr
16. git checkout -b sma-nrsur
17. pip install .
18. conda install -c conda-forge bilby_pipe

