## Unauthorized to use `gwdatafind`

*November 28, 2024*

I was trying to run parameter estimation on a GW event ont he CIT cluster usign `bilby_pipe`. However, I faced an issue- my run would fail at the `data_generation` step with the following (abbreviated) error-

```
File "/home/ish.gupta/.conda/envs/SMA/lib/python3.11/site-packages/requests/models.py", line 1021, in raise_for_status
    raise HTTPError(http_error_msg, response=self)
requests.exceptions.HTTPError: 401 Client Error: UNAUTHORIZED for url: https://datafind.igwn.org/LDR/services/data/v1/gwf/H.json
```
I think this essentially means that it would not allow me to use this data as I am not authorized, i.e., it does not recognize that I am a LIGO member and should be able to use this data. The way to overcome this is documented [here](https://git.ligo.org/pe/O4/o4a-rota/-/wikis/quick-bilby-tutorials) and reproduced:

```
$ kinit
$ export HTGETTOKENOPTS="-a vault.ligo.org -i igwn"
$ htgettoken
```

This should be executed before running `bilby_pipe`. Further, I also had to include `transfer-files=True` in my `bilby_pipe` `config.ini`. This worked!

*November 29, 2024*

When doing the same on the LLO cluster, I was still facing authorization issues. Including `getenv=[GWDATAFIND_SERVER]` (as is) in the `bilby_pipe` `config.ini` did the trick. This was borrowed from a [suggestion](https://chat.ligo.org/ligo/pl/eczjwxso4bfgdxoz8m3wtgj3ae) by Tomek Baka on LIGO Chat (Bilby Help channel) on August 14, 2024.