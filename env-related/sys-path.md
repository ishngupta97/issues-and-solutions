## sys.path

*Nov 26, 2021*

I am working on a project where I cloned igwn-py37-20211019 in CIT (env_name = plot_env) and then installed gwbench and its requirements. One of the requirements is `pandas=1.3.3`. I starting creating plotting scripts but would get an error about something going wrong in `pandas`. The original author of the scripts did not face any issues, so I checked the version of pandas being used.
```
import pandas as pd
print(pd.__version__)
```
This showed me that the version was different than the one I had installed. This basically meant that somehow some different pandas was being used instead of the one I had installed. This was confirmed when the output of
```
print(pd.__file__)
```
which gives the location of pandas package, was different from my env's site-packages. If the location appears in `PYTHONPATH`, start by cleaning it up,
```
export PYTHONPATH="" && source ~/.bashrc
```
 If cleaning the `PYTHONPATH` did not help either, this is where `sys.path` comes into the picture.
```
import sys
print(sys.path)
```
`sys.path` shows you all the places that the Python interpreter will look for the packages that you have imported. The output of the above command had multiple locations, icnluding the place from which panda was getting sourced. This location was that of a separate conda environment and had somehow creeped in in my `sys.path`. Hence, it needed to be removed.

The temporary fix was to use,
```
sys.path.remove('location-of-the-unnecessary-environment-being-used')
import pandas as pd
```
However, there might be a more permanent solution in some cases, and there was surely one in mine. `sys.path` is not only affected by `PYTHONPATH`, but it is also affected by two specific files- `easy-install.pth` `(/home/ish.gupta/.local/lib/python3.7/site-packages/easy-install.pth)`, and `setuptools` `(/home/ish.gupta/.local/lib/python3.7/site-packages/setuptools.pth)`. Any location mentioned in these two files will included in the `sys.path` and will superseed in priority the location specified by the current environment. I noticed that the undesired location in my case was present in  `setuptools.pth` and was absolutely unnecessary and could be removed. So, I removed the location from the file and things have worked perfectly since then. 