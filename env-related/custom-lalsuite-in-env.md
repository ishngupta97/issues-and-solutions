## Creating a conda environment with a custom LALSuite installation

*February 19, 2024*

I will follow the lalsuite installation tutorial by pycbc described [here](https://pycbc.org/pycbc/latest/html/install_lalsuite.html), with a few changes based on my experience.

1. Create a conda environment using
```
conda create -n SMA python=3.9
```
*Note: Check if this python version is still compatible.*

2. Activate the conda environment
```
conda activate SMA
```

3. Make a directory (called `src` here) in the environment folder which will store your lalsuite repo. Also create `opt` folder which will contain the installation of your lalsuite.
```
mkdir /home/ish.gupta/.conda/envs/SMA/src
mkdir /home/ish.gupta/.conda/envs/SMA/opt
```

4. Clone the lalsuite repo here
```
git clone git@git.ligo.org:higher-mode-amp-correction/rebased-code/lalsuite.git
```

5. Enter your lalsuite and checkout the desired branch. Then, follow the following steps to install lalsuite.
```
export IDIR=/home/ish.gupta/.conda/envs/SMA/opt/lalsuite
rm ${IDIR} -rf && ./00boot && ./configure --prefix=${IDIR} --enable-swig-python --enable-mpi --enable-openmp && make -j8 && make -j8 install
```
*Note 1: This step might give you an error saying that Numpy was not found. This is why I would also clone bilby and install its mandatory requirements to make sure that base packages are present. However, be careful when installing gw-requirements for bilby as it automatically installs lalsuite, which we don't want.*

*Note 2: While this worked perfectly well on CIT, the LLO cluster gave an error saying `configure: error: SWIG version 3.0.11 or later is required`. Checking with `swig -version` said that `swig` was not found. So, I installed swig using `pip install --force-reinstall -v "swig==3.0.12"` and re-submitted the command, and it seems to have worked this time.

6. Now, we make sure that the lalsuite is sourced everytime the environment is activated.
```
mkdir /home/ish.gupta/.conda/envs/SMA/etc
mkdir /home/ish.gupta/.conda/envs/SMA/etc/conda
mkdir /home/ish.gupta/.conda/envs/SMA/etc/conda/activate.d /home/ish.gupta/.conda/envs/SMA/etc/conda/deactivate.d
```
*Note- Always good to simply source the LALSuite anyway, with `source /home/ish.gupta/.conda/envs/SMA/opt/lalsuite/etc/lalsuite-user-env.sh`*


7. In `activate.d`, make `env_vars.sh, and inside it, write,
```
#!/bin/sh

source /home/ish.gupta/.conda/envs/SMA/opt/lalsuite/etc/lalsuite-user-env.sh
```

and in `deactivate.d`, make `env_vars.sh, and inside it, write,
```
#!/bin/sh

unset LAL_PREFIX
unset PYTHONPATH
```

The latter makes sure that the PTYHONPATH and the LAL_PREFIX are both unset when deactivating the environment.

You are done and can now deactivate your environment. When you reactivate, you can check if the correct LALSuite is being sourced with 
```
echo $LAL_PREFIX
```

*February 20, 2024*

I also wanted to install [bilby_pipe](https://lscsoft.docs.ligo.org/bilby_pipe/master/installation.html#installing-bilby-pipe-from-release) in the same environment. Turns out, when you do so (even from source) and run it, it completely ignores your LALSuite installation (at least on LLO) and just uses its own downloaded `python-lalsimulation`` and other such packages. Here are the possible solutions-

1. Recompile LALSuite after bilby_pipe has been installed (using step 5). *This did not work for me.*
2. Explicitly source your LALSuite installation with
```
source /home/ish.gupta/.conda/envs/SMA/opt/lalsuite/etc/lalsuite-user-env.sh
```
*This did not work for me.*

3. Include the following lines in your `config.ini` for bilby_pipe (under Job Submission Arguments, to be pedantic),
```
conda-env=/home/ish.gupta/.conda/envs/SMA
environment-variables={'HDF5_USE_FILE_LOCKING': 'false', 'OMP_NUM_THREADS': 1, 'OMP_PROC_BIND': 'false', 'LAL_PREFIX': '/home/ish.gupta/.conda/envs/SMA/opt', 'PYTHONPATH': '/home/ish.gupta/.conda/envs/SMA/opt/lalsuite/lib/python3.9/site-packages'}
```
This did work for me.
