## LAL_DATA_PATH

*February 17, 2023*

I had created a `conda` environment with an installation of [bilby](https://git.ligo.org/lscsoft/bilby/-/tree/master/) for one of the projects. As a part of it, I wanted to use `ROM` waveforms. To use these waveforms, additional files need to be downloaded, which can be done by cloning [lalsuite-extra](https://git.ligo.org/lscsoft/lalsuite-extra/). To run bilby with the ROM waveforms, one needs to add the local path of `lalsuite-extra/data/lalsimulation` to `LAL_DATA_PATH`. I first tried to just use-
```
export $LAL_DATA_PATH = /local/path/to/dir
```
but that did not seem to work. Then, I was pointed to a better method, which involved [saving environment variables for the conda environment](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html#macos-and-linux). I will reiterate the steps here (macOS and Linux)-

1. Obtain the path to your conda environment
```
echo $CONDA_PREFIX
```
2. Enter the folder and make the following directories
```
cd $CONDA_PREFIX
mkdir -p ./etc/conda/activate.d
mkdir -p ./etc/conda/deactivate.d
touch ./etc/conda/activate.d/env_vars.sh
touch ./etc/conda/deactivate.d/env_vars.sh
```
3. Edit `./etc/conda/activate.d/env_vars.sh` as follows:
```
#!/bin/sh

export LAL_DATA_PATH=/local/path/to/dir
```
3. Edit `./etc/conda/deactivate.d/env_vars.sh` as follows:
```
#!/bin/sh

unset LAL_DATA_PATH
```

Now, when you activate the environment, the `LAL_DATA_PATH` will be set automatically. Deactivating the environment will remove the set `LAL_DATA_PATH`.
