# Issues and solutions

This is the place where I document the problems I face during code development and how (if) they were resolved. 

Note that this is not technical advice in any shape or form. This documentation exists for two reasons only-

1. For me to know what to do if I face the same problem again.
2. For you to know what worked for me, because it might work for you.

If you have better solutions to the problems I faced, or if you want to point out something that I stated incorrectly (which is entirely possible), then please feel free to contact me at [ishgupta@psu.edu](mailto:ishgupta@psu.edu).

Hope this helps :)
