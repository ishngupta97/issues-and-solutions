### Working with Jupyter on the ICS cluster

*May 3, 2023*

The ICS cluster at Penn State does not have in-built jupyter, and I, like most others, thought that I could never use Jupyter Lab on the ICS cluster. Thanks to Rahul Kashyap, this is no longer the case. The general idea is to use `tmux` or `screen` and run the jupyter session there. Then, access it remotely using a `ssh` command. Here are the steps-

- Source conda- `source /cvmfs/oasis.opensciencegrid.org/ligo/sw/conda/etc/profile.d/conda.sh`
- Activate your conda env- `conda activate myenv`
- Install jupyter if not already present (see instructions [here](https://gitlab.com/ishngupta97/issues-and-solutions/-/blob/main/jupyter-related/installation.md))
- Generate config- `jupyter notebook --generate-config`. This only needs to be done the first time.
- Set a password- `jupyter notebook password`. This also only needs to be done the first time. It is also important to do so so that other people cannot access your jupyter session.
- Run jupyter in the `no-browser` mode- `jupyter lab --no-browser --port=8889`.

The port number given here is for reference only. If this port is not available, the terminal will say the following- 
```
The port 8889 is already in use, trying another port.
Jupyter Server 2.5.0 is running at:
http://localhost:8890/lab
http://127.0.0.1:8890/lab
```
This means that, as `port=8889` was busy, jupyter is now running on `port=8890`. This port number is important, as it will be required to access the jupyter session from the local machine.

Now, on the local machine, use
```
ssh -N -L localhost:8887:localhost:8890 user@remote-ssh
```
There are several parts to it.
- `localhost:8887` chooses the `port=8887` on the local machine. You can choose a different number if you want.
- `localhost:8890` refers to the port on the ICS cluster where the jupyter session is running. This is why remembering the port number in the previous steps was important. As you can see, it is the same port number that was assigned when you started the jupyter session.
- `user@remote-ssh` is where your ssh ID goes. For ICS, this would be of the form `user.name@ligo01.ics.psu.edu`.

Now, on your local machine, go to a web browser and enter
```
localhost:8887/lab
```
See that this is the same port number as you chose for you local machine in the previous command. 

Entering this will prompt you for the password that you set before launching the session on ICS, and then, you are in!
