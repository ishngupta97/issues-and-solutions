### Cannot find LaTeX

I was using jupyter on my remote system and trying to make plots. The kernel was the one corresponding to my conda enviroment. However, I kept getting the error,
```
Failed to process string with tex because latex could not be found.
```
In this case, the first thing to do is to make sure that you download MacTeX (as I am using Mac OS). This can be done [here](https://tug.org/mactex/). Make sure to follow the steps on the page that are required to ascertain that it was installed correctly (i.e. check the `md5` step). Even then, my jupyter lab failed to find latex. This can be seen by going to the terminal through jupyter launcher and typing `pdflatex` and pressing return. An error message tells you that it does not recognise the command. I also did,
```
pip install latex
```
but to no avail. What finally worked is mentioned in the answer [here](https://stackoverflow.com/a/52498741). I need to add the path to my MacTeX insatllation to my `PATH`. For the same, I included,
```
### TeX
export PATH="/Library/TeX/Distributions/.DefaultTeX/Contents/Programs/texbin:$PATH"
```
to my `.zshrc` file (the zsh counterpart to .bash_profile, as I use zsh). and sourced it (source ~/.zshrc). I then did `echo $PATH` and made sure that the path was indeed updated. Finally, reran the plotting commands in juopyter and everything seemed to work. 
