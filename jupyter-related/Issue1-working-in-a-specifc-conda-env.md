### Working in a specific conda environment

*Nov 26, 2021*

If you are working natively, this is easy to do. Just launch jupyter lab after you have activated the conda environment and things will workout by themselves.

However, on `jupyter2.ligo.caltech.edu`, jupyter lab always opens up in the base conda environment. To shift to your environment and work with ipython, you first need to create a ipython kernel. On the terminal, activate the necessary conda environment and then execute ([source](https://stackoverflow.com/a/44786736))
```
python -m ipykernel install --user --name myenv --display-name "Python (myenv)"
```
Now, refresh the jupyter lab and go to the working file. In the top-right corner, switch the kernel from what it is by default (probably something like Python 3) to `Python (myenv)`. Now, things should work out the way you want them to.
