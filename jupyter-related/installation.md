## Installation

[Great acrticle](https://towardsdatascience.com/how-to-set-up-anaconda-and-jupyter-notebook-the-right-way-de3b7623ea4a) on how to install Jupyter in a conda environment. Basic steps-
```
conda install -c conda-forge jupyterlab notebook nb_conda_kernels ipykernel

```

I prefer working on Jupyter lab over Jupyter notebook. To make sure that you can work in a particular environment on jupyterlab, use this- 

```
python -m ipykernel install --user --name myenv --display-name "Python (myenv)"
```

If there are issues in working on Jupyter lab natively (as I faced for the nsbh project), I switched to jupyter lab by ligo.

```
jupyter2.ligo.caltech.edu
```