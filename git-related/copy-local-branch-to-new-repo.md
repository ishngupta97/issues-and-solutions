## Copy local branch to new repository

*October 24, 2023*

I wanted to rebase a particular repository, say A, which is a fork of B. I want A to be up-to-date with B. However, to not mess things up, I made a copy of A (called A-copy) by forking A and then removed the fork relationship of A-copy with A. I tried rebasing A-copy to make it up to date with B and that worked. TO do this, I simply added B as the `upstream` for A-copy and rebased. 

Now that I know that the rebase has worked, I want to copy a particular branch of A-copy (this branch has also been rebased) to A. To do this, I need to add A-copy as the upstream to A, and then create a new branch in A copying the branch in A-copy. This is how I did it:

1. git remote add upstream https://github.com/A-copy.git (use the SSH one)
2. git fetch upstream
3. git checkout -b <my_new_branch> upstream/<name_of_branch_to_be_copied>
4. git push -u origin <my_new_branch>

If a remote `upstream` already exists and is different from the one you want to add (this can be viewed using `git remote -v`), then first remove that `upstream` and then add the desired one:

1. git remote rm upstream
2. git remote add upstream https://github.com/new-upstream.git
