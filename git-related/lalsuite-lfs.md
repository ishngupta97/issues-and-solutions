## The LFS issue when rebasing LALSuite

*October 24, 2023*

I had to rebase an existing fork of lalsuite to get it on level with the master. For the same, I followed these steps to rebase my master:

1. Go to lalsuite.
2. git remote add upstream https://github.com/original-repo/goes-here.git (use the SSH one)
3. git fetch upstream
4. git checkout master
5. git rebase -i upstream/master

However, the rebase wouldn't suceed and it gave me an error that a particular `lalpulsar` file was either not found or couldn't be accessed. I thought the issue was that this file must be added using `git lfs`, but the files in `git lfs` with upstream were different from the files of the repo that was being rebased, and the upstream ones were not being automatically downloaded. So, instead, one should first fetch the upstream lfs and then do the rebasing. Specifically,

1. git remote add upstream https://git.ligo.org/lscsoft/lalsuite.git
2. git fetch upstream
3. git lfs fetch upstream upstream/master
4. git lfs pull
5. git lfs install --force (Not sure if 4 and 5 are actually needed, but that's what I did)
6. git rebase -i upstream/master

This worked!
